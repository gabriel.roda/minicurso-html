let a = 1,
    b = 1,
    hip = 0;

//1. Os inputs ".lado-a" e ".lado-b" chamam este método quando o valor deles é alterado
// ele deve atualizar o valor da variável "a" ou "b", dependendo do parâmetro "queLado"
// com o valor "queValor" (ele deve ser convertido para número)
function atualizaLado(queLado, queValor) {
    // -- inserir o código aqui



    // -- --
    atualizaHipotenusa();
    atualizaArea();
}

//2. O método atualizaHipotenusa() vai calcular o valor da hipotenusa, caso as variáveis "a" e "b" tenham valores válidos, e,
// após isso, jogar este valor no input ".hipotenusa"
function atualizaHipotenusa() {
    // -- inserir o código aqui



    // -- --
}

//3. Caso "a" e "b" sejam valores válidos, calcular a área do triângulo e mostrar na página.
// para isso é preciso remover a classe "escondido" do "p" já presente na página e inserir o valor
// na span dentro dele.
function atualizaArea() {
    // -- inserir o código aqui



    // -- --
}

//bonus
//4. calcular os ângulos internos do triângulo ao atualizar os lados.
// os elementos já foram criados e posicionados na localização correta, é só procurar no html.
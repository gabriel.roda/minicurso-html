/**
*   ? querySelector
*       * style
*       * classList
*       * setAttribute()
*
*   ? onclick
*   ? onchange
*/

let cabecalho = document.querySelector(`#cabecalho-principal`);

cabecalho.style.color = "#000"
cabecalho.style.fontSize = `36px`
cabecalho.style.fontFamily = `Comic Sans MS`

cabecalho.classList.remove(`um-cabecalho`)
cabecalho.classList.add(`um-cabecalho-javascript`, `mais-uma-classe`)

document.querySelector(`.meu-flex a`).setAttribute(`title`, `Novo title aplicado por JS`)



function minhaFunc() {
    let divDiego = document.querySelector(`.diego`);

    divDiego.style.background = "#ff00ff"
    divDiego.classList.add(`adicionei-uma-classe`)
}

function atualiza() {
    let n = document.querySelector(`.input-number`).value;

    document.querySelector(`.resultado-op`).innerHTML =
        `        O valor do input number é de    ` + n
}


document.querySelector('.input-range').setAttribute('oninput', 'atualizaInput(this)')

function atualizaInput(element) {
    let n = element.value;

    document.querySelector(`.input-number`).value = n;

    window.localStorage.setItem(`meu-valor`, n);
}

document.querySelector(`.input-range`).value = window.localStorage.getItem(`meu-valor`);
document.querySelector(`.input-number`).value = window.localStorage.getItem(`meu-valor`);
//1. defina uma variável "anoNascimento" e atribua a ela o ano em que você nasceu

//2. defina uma variável "esseAno" com o ano atual (2020)

//3. defina uma variável "completou18em" e atribua a ela o ano em que você
//completou/completará 18 anos (o valor deve ser calculado)

//4. define uma variável maiorDeIdade e atribua a ela o valor booleano que diz se você é ou não maior de idade
// (o valor deve ser calculado)

//5. Você deve escrever uma função que ajuda o porteiro do Matahari, defina a função
// eMaiorDeIdade(ano) que retorna um valor booleano, dizendo se a pessoa nascida em "ano" é
// ou não maior de idade

//6. Você deve iterar sobre o vetor "pessoas" e dizer quem é ou não maior de idade,
//as mensagens devem ser printadas no console (Console.log)
//ex. "José é maior de idade"
let pessoas = [
    { nome: "José", ano: 1990 },
    { nome: "Maria", ano: 2009 },
    { nome: "João", ano: 1995 },
    { nome: "Ana", ano: "2004" },
    { nome: "Adriana", ano: "2002" }
];

//7. defina uma função que calcule a hipotenusa de um triângulo, dados os lados 'a' e 'b'

//8. defina uma função que calcule a área de um triângulo, dados os lados 'a' e 'b'

//9. calcule a hipotenusa e a área dos seguintes triângulos
// para cada triângulo mostrar uma mensagem seguindo este padrão:
// "O triângulo 0 tem uma hipotenusa de tamanho e área "
let triangulos = [
    { a: 5, b: 7 },
    { a: 12, b: 7 },
    { a: 50, b: 4 },
    { a: 2, b: 2 },
    { a: 5, b: 1 },
    { a: 3, b: 6 },
    { a: 9, b: 12 },
    { a: 25, b: 2 }
];

//-- bonus --
//10. defina uma função que tome como entrada a hipotenusa e um ângulo de um triângulo reto,
//e que retorne um objeto com o tamanho dos lados a, b e c e a área
//as funções Math.sin e Math.cos funcionam só com radianos, a fórmula abaixo converte graus para radianos
function grausParaRadianos(deg) {
    return deg * Math.PI / 180
}
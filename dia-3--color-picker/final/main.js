let r, g, b;

function atualizaR(valor) {
    r = valor;
    document.documentElement.style.setProperty("--r", valor);

    document.querySelector('#range-r').value = valor;
    document.querySelector('#num-r').value = valor;

    atualizaCopia();
}

function atualizaG(valor) {
    g = valor;
    document.documentElement.style.setProperty("--g", valor);

    document.querySelector('#range-g').value = valor;
    document.querySelector('#num-g').value = valor;

    atualizaCopia();
}

function atualizaB(valor) {
    b = valor;
    document.documentElement.style.setProperty("--b", valor);

    document.querySelector('#range-b').value = valor;
    document.querySelector('#num-b').value = valor;

    atualizaCopia();
}

function init() {
    atualizaR(127)
    atualizaG(127)
    atualizaB(127)
}

function atualizaCopia() {
    document.querySelector('#rgb').value = `rgb(${r}, ${g}, ${b})`;
    document.querySelector('#hex').value = `#${rToH(r)}${rToH(g)}${rToH(b)}`
}

function rToH(num) {
    let h = Number(num).toString(16);
    if (h.length < 2) {
        h = "0" + h;
    }
    return h;
}

window.addEventListener('load', init);
Ementa do minicurso de HTML

# Dia 1 - HTML e CSS

No primeiro dia vamos focar na apresentação da página, aprendendo e aplicando conceitos de HTML e CSS.

## HTML

O HTML é a estrutura física da página.
Ele nos diz quais elementos estão presentes, em que ordem e em qual hierarquia.

### Uma simples página

Vamos criar um post de blog, com imagens, tabelas, listas e links para outras páginas.

- Vamos adicionar os seguintes elementos à uma página para ver como eles se comportam:

  - Div
  - Títulos
  - Parágrafos
  - Links
  - Imagens
  - Listas
  - Tabelas

### Atributos de uma tag

- Depois de termos nos familiarizado com as principais tags, vamos editar alguns atributos:

  - Href
  - Src
  - Style
  - Alt
  - Title
  - Id
  - Class

---

### Atividades + Correção

---

## CSS

O CSS é o estilo das nossa páginas.
Podemos alterar fontes, tamanhos, posicionamento entre outros atributos.

### Seletores simples – id e classe

- Aplicando regras CSS a elementos específicos, restringindo a seleção através do ID ou classe

### Seletores com mais de uma classe, filhos

- Aplicando regras CSS mais abrangentes, a vários elementos de um vez com um só seletor

### Propriedades

- Principais proprieades CSS

  - Background
  - Color
  - Font-family
  - Font-size
  - Line-height
  - Font-weight

### Tamanhos

- box-sizing
- margin
- border
- padding
- box-shadow

---

### Atividades + Correção

---

# Dia 2 - JavaScript

## Variáveis

- Guardando valores na memória
- Realizando operações com os valores

## Funções

- Declaração e chamada de funções
- `return`

## Condições, loops

- Realizando uma operação com base em uma condição
  - `if/else`
  - `switch`
- Realizando uma operação mais de uma vez
  - `for`
  - `while`

## Console.log

- Mostrando o resultado das operações em algum lugar

---

### Atividades + Correção

---

## Document.querySelector

- Selecionando elementos do meu documento com seletores CSS
- `.setAttribute()`
- `.style`
- `.classList`
  - `.add`
  - `.remove`
  - `.toggle`

## Eventos

- `onclick`
- `onchange`

---

### Atividades + Correção

---

# Dia 3 - Montando o Color Picker

![alt](https://uniasselvi-digital.web.app/fin.png)

## Revisão

Vamos tirar as dúvidas que ficaram dos dois primeiros dias e em seguida, criar o color picker

## Atividade - Criação do Color Picker

## Resolvendo Juntos

1. div.color
2. sliders
3. valores
4. min max step
5. label
6. centralizar
7. alterar background oninput
8. copia rgb
9. copia hex
10. ranges customizados
11. editando variáveis css por js
